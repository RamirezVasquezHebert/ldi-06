
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hebert
 */
public class Complemento_dos {
    public static void main(String args[]) {                
     
          int[] a1 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el número binario")));
           int[] b2 ={0,0,0,0,0,0,0,1};
          System.out.println("Valor inicial");
          for (int item : a1) {
            System.out.print(item);
        }                   
        
        System.out.println();
        
        int invert[] = invertir(a1);
         System.out.println("Valor invertido");
        for(int i = 0; i < invert.length; i++) {
            System.out.print(invert[i]);
            
        }
        System.out.println();
        
        
         int sum[] = Suma1(invert, b2);
         
          System.out.println("formar el complemento a dos sumando 1");
         
        for(int i = 0; i < sum.length; i++) {
            System.out.print(sum[i]);
            
        }
       
        System.out.println();
     }
     
      public static int[] rellenar (int numero){
        String binario = "";
        binario = numero + binario;               
        
        int resultado [] = new int[8];
        for(int i = resultado.length-1 , j=0, k=0; i > -1; i--, j++) {
            
            if(i >= binario.length()) {
                resultado[j] = 0;
            } else {
                
                resultado[j] = Integer.parseInt(binario.charAt(k)+"");
                k++;
            }
        }return resultado;
      }
     
     public static int[] invertir(int a[]) {
         
         
        int[] c = new int[a.length];
       
        for (int i = a.length - 1; i > -1; i--) {            
            int invert = a[i]; 
            if(invert == 0) {
               c[i] = 1;
               
           }
            
            
            
            else if(invert == 1) {
               c[i] = 0;
           }
           
           
          
        }
        return c;//Se regresa el arreglo.
        
     }
     
      public static int[] Suma1(int a[], int b[]) {
        int acarreo = 0;//variable de control.
        int[] c = new int[a.length];//Arreglo a retornar.        
        //Se recorre el arreglo a de manera inversa.
        for (int i = a.length - 1; i > -1; i--) {            
            int suma = a[i] + b[i]; //Se realiza  la suma una vez por ciclo
            if(suma == 2) {//Si la suma da 2 los elementos en el arreglo son 1 y 1.
               c[i] = 0;//Por lo tanto debe ir como resultado un 0
               acarreo = 1;//Y se lleva un acarreo
           }
            
            
            //Si por el contrario la suma es 0 quiere decir que los elementos
            //eran 0 y 0.
            else if(suma == 0) {
               c[i] = 0;//Entonces debe ir 0 como resultado
           }
            //Si por el contrario la suma es 1, entonces los elementos 
            //eran 0 y 1 o viceversa.
            else if(suma == 1) {
               c[i] = 1;//Y debe ir como resultado un 1.
           }
           if(acarreo == 1) {//Si existe un acarreo en este ciclo
               //Se comprueba si la suma del elemento siguiente del
               //arreglo "a" mas el acarreo es igual a 2.
               if(a[i-1] + 1 == 2) {
                   a[i-1] = 0;//Si lo es, se asigna un 0 a "a".
                   acarreo = 1;//Y se sigue llevando un acarreo.
               } else {//Si la suma no da 2, solo puede dar 1.
                   a[i-1] = 1;//Y entonces se asigna 1 a "a".
                  acarreo = 0;//Y el acarreo desaparece.
               }
           } 
        }
        return c;//Se regresa el arreglo.
    }
     
    

    
}
